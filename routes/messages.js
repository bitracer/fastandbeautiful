var express = require('express');
var store = require('../services/messageStore');
var app = express();

app.post('/message', function(req, res){
    "use strict";
    var data = {
        success: false
    };
    store.add(req.body, function(err, newDoc){
        if(err){
            data.message = err;
            data.success = false;
        } else {
            data.result = newDoc;
            data.success = true;
        }
        res.json(data);
    })
});
app.get('/allmessages', function(req, res){
    "use strict";
    var data = {
        success: false
    };
    store.getAll(function(err, docs){
        if(err){
            data.success = false;
            data.message = err;
        } else {
            data.result = docs;
            data.success = true;
        }
        res.json(data);
    })
});

module.exports = app;

