var express = require('express');
var store = require('../services/curveStore');
var app = express();

app.post('/curve', function(req, res){
    "use strict";
    var data = {
        success: false
    };
    store.add(req.body, function(err, newDoc){
        if(err){
            data.success = false;
            data.message = err;
        } else {
            data.success = true;
            data.result = newDoc
        }
        res.send(data);
    });
});

app.get('/curves', function(req, res){
    "use strict";
    var data = {
        success: false
    };
    store.getSince(-1, function(err, docs){
        if(err){
            data.success = false;
            data.message = err;
        } else {
            data.success = true;
            data.docs = docs;
        }
        res.send(data);
    })
});

app.get('/allcurves', function(req, res){
    "use strict";
    var data = {
        success: false
    };
    store.getAll(function(err, docs){
        if(err){
            data.success = false;
            data.message = err;
        } else {
            data.success = true;
            data.docs = docs;
        }
        res.send(data);
    })
});

app.patch('/reset', function(req, res){
    "use strict";
    var data = {
        success: false
    };
    store.reset();
    data.success = true;
    res.send(data);
});

module.exports = app;