var express = require('express');
var app = express();
var currentPowerLevel = 0;
app.post('/set', function(req, res){
    setPowerLevel(parseInt(req.body.powerLevel));
    res.json({
        powerLevel: getPowerLevel(),
        success: true
    });
});

app.get('/get', function(req, res){
    var result = {
        success: true,
        powerLevel: getPowerLevel()
    };
    res.json(result);
});
function getPowerLevel(){
    return currentPowerLevel;
}
function setPowerLevel(level){
    currentPowerLevel = level;
}

module.exports = app;