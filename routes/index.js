var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  //res.render('index', { title: 'Fast and Beautiful' });
  res.sendfile('public/static.html');
});

module.exports = router;
