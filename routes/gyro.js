var express = require('express');
var app = express();
var currentGyroAccelleration = 0;

app.post('/set', function(req, res){
    setGyroAcceleration(parseInt(req.body.gyroLevel));
    res.json({
        success: true,
        gyroLevel: getGyroAcceleration()
    });
});

app.get('/get', function(req, res){
    res.json({
        success: true,
        gyroLevel: getGyroAcceleration()
    });
});

function setGyroAcceleration(gyroValue){
    currentGyroAccelleration = gyroValue;
}
function getGyroAcceleration(){
    return currentGyroAccelleration;
}
module.exports = app;
