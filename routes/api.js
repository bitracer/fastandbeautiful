var express = require('express');
var curve = require('./curves');
var message = require('./messages');
var power = require('./power');
var gyro = require('./gyro');
var app = express();

app.use('/curve', curve);
app.use('/message', message);
app.use('/power', power);
app.use('/gyro', gyro);
module.exports = app;