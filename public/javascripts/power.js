$(function(){
    var MAX_POWER = 300;
    function getData(){
        $.ajax({
            url: 'api/power/get',
            success: function(result){
                updateChart(result);
            },
            error: function(result){
                updateChart(result);
            }
        })
    }
    function updateChart(data){
        var powerLevel = parseInt(data.powerLevel);
        var negativeLevel = MAX_POWER - powerLevel;
        chart.segments[0].value = powerLevel;
        chart.segments[1].value = negativeLevel;
        chart.update();
    }
    function drawChart(options) {
        var ctx = $('#power_level').get(0).getContext('2d');
        var pattern = [
            {
                value: 1,
                color: "#13AE00",
                highlight: "#14DA00",
                label: "Power"
            },
            {
                value: 299,
                color: "#AAAAAA",
                highlight: "#AAAAAA"
            }
        ];
        var powerLevel = new Chart(ctx).Doughnut(pattern, options);
        return powerLevel;
    }
    function onError(error){
        //TODO: implement
    }
    var refresh = true;
    var chart = drawChart({});
    if(refresh) setInterval(getData, 1000);
});
