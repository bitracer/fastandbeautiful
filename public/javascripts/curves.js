"use strict";
$(function(){
    function getCurves(){
        $.ajax({
            url: "/api/curve/allcurves",
            success: function(result){
                prepareCurves(result);
            }
        });
    }
    function prepareCurves(result){
        result.docs = (result.docs).sort(compareCurves);
        var curves = result.docs;
        curves.forEach(function(entry){
            var timestamp = parseInt(entry.timePassedSinceStart/10);
            var time = secToTime(timestamp);
            entry.minPassedSinceStart = time.min;
            entry.secPassedSinceStart = time.sec;
        });
        renderCurves(result);
    }
    function secToTime(int){
        var min = 0, sec = int;
        while(sec > 60){
            min++;
            sec = sec-60;
        }
        return {min: min, sec: sec};
    }
    function compareCurves(a, b){
        if(parseInt(a.timePassedSinceStart, 10) < parseInt(b.timePassedSinceStart, 10)){
            return -1;
        }
        if(parseInt(a.timePassedSinceStart, 10) > parseInt(b.timePassedSinceStart, 10)){
            return 1;
        }
        return 0;
    }
    function renderCurves(result){
        var curveTemplateScript = $("#curve_template").html();
        var curveTemplate = Handlebars.compile(curveTemplateScript);
        $('#curve_container').html(curveTemplate(result));
    }
    function resetCurves(){
        console.log("Reset");
        $.ajax({
            method: "PATCH",
            url: "/api/curve/reset",
            success: function(result){
                var placeholder = $("#reset_result");
                setTimeout(function(){
                    placeholder.text("");
                }, 5000);
                placeholder.text("Reset");
            }
        });
    }
    $("#reset").click(function(){
        resetCurves();
    });
    $("#refresh").click(function(){
        getCurves();
    });
    getCurves();
    var refresh = true;
    refresh ? setInterval(getCurves, 1000) && $("#refresh").hide(): null;
});