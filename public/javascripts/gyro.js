$(function(){
    function getData(){
        $.ajax({
            url: 'api/gyro/get',
            success: function(result) {
                updateChart(result);
            },
            error: function(result){
                onError(result);
            }
        });
    }
    function updateChart(newData){
        var gyroValue = parseInt(newData.gyroLevel);
        chart.datasets[0].points[0].value=gyroValue;
        chart.update();
    }
    function drawChart(options){
        var ctx = $('#gyro_acceleration').get(0).getContext('2d');
        var data = {
            labels: ["Gyro-Z Value"],
            datasets: [
                {
                    label: "",
                    fillColor: "#A39100",
                    highlightFill: "#A38903",
                    data: [0]
                }
            ]
        };
        var barChart = new Chart(ctx).Line(data, options);
        return barChart;
    }
    function onError(result){

    }
    var refresh = true;
    var options = {
        scaleOverride: true,
        scaleStartValue:-100,
        scaleStepWidth: 100,
        scaleSteps: 200,
        scaleShowLabels: false,
    };
    var chart = drawChart(options);
    if(refresh) setInterval(getData, 1000);
});
