"use strict";
$(function(){
    function getMessages(){
        $.ajax({
            url: "/api/message/allmessages",
            success: function(result){
                renderMessages(result);
            }
        });
    }
    function renderMessages(result){
        var messageTemplateScript = $('message_template').html();
        var messageTemplate = Handlebars.compile(messageTemplateScript);
        $('message_container').html(messageTemplate(result));
    }
});
