#Fast and Beautiful
A NodeJS website which visualizes the gathered and analyzed data from the Bitracer project.

## Usage
It is the default NodeJS setup procedure:

```
npm install
node bin/www
```

