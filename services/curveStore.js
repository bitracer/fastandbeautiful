var Datastore = require('nedb');
var db = new Datastore();

/**
 * Takes a JSON formatted curve object with a timestamp, to sort their occurence
 * @param curve
 * @param timestamp
 * @return newDoc New created document
 */
function addCurve(curve, callback){
    db.insert(curve, callback);
}
/**
 * Recieve all curve objects since timestamp
 * @param timestamp
 * @return docs array of cuves
 */
function getCurves(time, callback){
    db.find({ timestamp: { $gt: time }}, callback);
}

function getAllCurves(callback){
    db.find({}, callback);
}
function newDatastore(){
    db = new Datastore();
}
function saveDatastore(file, callback){
    var save = new Datastore({filename: file});
    getAllCurves(function(err, data){
        if(err) throw err;
        for(entry in data){
            save.insert(entry, callback);
        }
    });
}
module.exports = {add: addCurve, getSince: getCurves, getAll: getAllCurves, reset: newDatastore, save: saveDatastore};