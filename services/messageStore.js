var Datastore = require('nedb');
var db = new Datastore();

function addMessage(message, callback){
    db.insert(message, callback);
}
function getMessages(time, callback){
    db.find({ timestamp: { $gt: time }}, callback);
}
function getAllMessages(callback){
    db.find({}, callback);
}
function newDatastore(){
    db = new Datastore();
}

module.exports = {add: addMessage, getSince: getMessages, getAll: getAllMessages, reset: newDatastore};